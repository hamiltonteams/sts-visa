<?php
/**
 * The template used to load the Top Navbar Menu in header*.php
 *
 * @package Alien Ship
 */
?>
<!-- Top Menu -->
	<div class="header">
		<div class="nav-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-lg-4">
						<div class="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo ot_get_option('logo') ;?>" alt="Logo"></a>
						</div>
					</div>
					<div class="col-sm-8 col-lg-8">
						<div class="lien-he">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="#">TƯ VẤN: MR. HOÀNG - <span>0919.324.052</span></a></li>
								<li><a href="#">MS. TUYẾN - <span>0934.441.879</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="<?php echo apply_filters( 'alienship_top_navbar_class' , 'navbar navbar-inverse top-navigation' ); ?>" role="navigation">
			<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a>
			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<?php wp_nav_menu( array(
					'theme_location' => 'top',
					'depth'          => 2,
					'container'      => false,
					'menu_class'     => 'nav navbar-nav',
					'walker'         => new wp_bootstrap_navwalker(),
					'fallback_cb'    => 'wp_bootstrap_navwalker::fallback'
					)
				); ?>
				<ul class="nav navbar-nav navbar-right social">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
		</nav>
	</div>
<!-- End Top Menu -->
