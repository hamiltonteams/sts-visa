<?php
/**
 * Template Name: Home Page
 * The template for displaying full-width pages with no sidebar.
 *
 * @package Alien Ship
 */

get_header(); ?>
	<div id="primary" class="col-sm-12 col-lg-12">

		<?php echo do_shortcode('[rev_slider home-slider]') ;?>
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			do_action( 'alienship_loop_before' );

			get_template_part( '/templates/parts/content', 'home' );

			do_action( 'alienship_loop_after' );

		endwhile; ?>

		</main><!-- #main -->

	</div><!-- #primary -->
<?php get_footer(); ?>
